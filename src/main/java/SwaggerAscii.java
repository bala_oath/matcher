import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * @author balamurugan
 */
public class SwaggerAscii {
	public static void main(String[] args) throws MalformedURLException {
		Swagger2MarkupConverter.from(new URL("http://localhost:8080/v2/api-docs"))
				.withConfig(new Swagger2MarkupConfigBuilder()
						.withMarkupLanguage(MarkupLanguage.ASCIIDOC)
						.build())
				.build().toFolder(Paths.get("/Users/balamurugan/dev/analysis/kite"));
	}
}
