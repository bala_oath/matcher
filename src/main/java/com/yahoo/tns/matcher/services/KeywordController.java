package com.yahoo.tns.matcher.services;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author balamurugan
 */
@RestController
@RequestMapping("/keywords")
public class KeywordController {
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long createSegment(@RequestBody Segment segment) {
		return 0L;
	}
}
