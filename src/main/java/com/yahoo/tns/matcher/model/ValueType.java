package com.yahoo.tns.matcher.model;

/**
 * @author balamurugan
 */
public enum ValueType {
	EXACT, IGNORE_CASE, IGNORE_ACCENT, IGNORE_CASE_AND_ACCENT;
}
