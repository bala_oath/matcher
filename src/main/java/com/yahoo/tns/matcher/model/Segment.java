package com.yahoo.tns.matcher.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * @author balamurugan
 */
@Entity
public class Segment {
	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true)
	private String name;

	@Column
	private long accountId;

	@Column
	private String adsystem;

	@ManyToMany
	@JoinTable(name = "dataset_segment", joinColumns = @JoinColumn(name = "dataset_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "segment_id", referencedColumnName = "id"))
	private List<Dataset> datasets;
}
