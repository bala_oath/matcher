package com.yahoo.tns.matcher.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author balamurugan
 */
@Entity
public class RootWord {
	@Id
	@GeneratedValue
	private long id;

	@OneToOne
	@JoinColumn(name = "keyword_id")
	private Keyword keyword;

	@Column
	private String rootWord;
}
