package com.yahoo.tns.matcher.model;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author balamurugan
 */
@RepositoryRestResource
public interface KeywordRepository extends PagingAndSortingRepository<Keyword, Long> {
}
