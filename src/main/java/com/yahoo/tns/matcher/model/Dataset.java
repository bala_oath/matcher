package com.yahoo.tns.matcher.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author balamurugan
 */
@Entity
public class Dataset {
	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true)
	private String name;

	@Column
	private String locale;

	@Column
	private boolean isActive;
}
