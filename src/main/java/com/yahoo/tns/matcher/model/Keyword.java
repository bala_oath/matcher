package com.yahoo.tns.matcher.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author balamurugan
 */
@Entity
public class Keyword {
	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	@JoinColumn(name = "segment_id")
	private Segment segment;

	@Column
	private String keyword;

	@Enumerated(EnumType.ORDINAL)
	private ValueType valueType;

}
